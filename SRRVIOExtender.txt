Board Information Report
Filename     : C:\Users\Aaron\Desktop\SRRVIOExtender\SRRVIOExtender.PcbDoc
Date         : 2/20/2020
Time         : 7:54:44 PM
Time Elapsed : 00:00:00

General
    Board Size, 1980milsx500mils
    Components on board, 2
count : 2

Routing Information
    Routing completion, 88.89%
    Connections, 36
    Connections routed, 32
    Connections remaining, 4
count : 4

Track Width, Count
    1mils, 4
    1.969mils, 8
    5mils, 20
    6.851mils, 51
    6.851mils, 11
    7mils, 57
    10mils, 50
count : 7